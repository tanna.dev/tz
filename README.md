# `tz`

A command-line tool which provides an interactive interface for determining timezone compatibility i.e. for meetings.

## Install

```sh
go install gitlab.com/tanna.dev/tz@latest
```

## Usage

Run `tz` and then, interactively:

- <kbd>q</kbd> quits the program
- <kbd>space</kbd> (un)selects a timezone to compare
- <kbd>=</kbd> rounds down to the nearest hour
- <kbd>↑</kbd> / <kbd>k</kbd> go up
- <kbd>↓</kbd> / <kbd>j</kbd> go down
- <kbd>→</kbd> +15 minutes
- <kbd>←</kbd> -15 minutes
- <kbd>?</kbd> shows help menu

## Demo

See the blog post [_Making it easier to schedule cross-timezones, with the `tz` CLI_](https://www.jvt.me/posts/2024/05/15/tz/) for a demo.

## License

Apache-2.0.
