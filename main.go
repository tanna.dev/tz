package main

import (
	"fmt"
	"os"
	"time"

	"github.com/charmbracelet/bubbles/help"
	"github.com/charmbracelet/bubbles/key"
	"github.com/charmbracelet/bubbles/table"
	tea "github.com/charmbracelet/bubbletea"
)

type keyMap struct {
	Left  key.Binding
	Right key.Binding
	Up    key.Binding
	Down  key.Binding
	Equal key.Binding
	Space key.Binding
	Quit  key.Binding
	Help  key.Binding
}

func (k keyMap) ShortHelp() []key.Binding {
	return []key.Binding{k.Help, k.Quit}
}

func (k keyMap) FullHelp() [][]key.Binding {
	return [][]key.Binding{
		{k.Up, k.Down, k.Left, k.Right},
		{k.Space, k.Equal},
		{k.Help, k.Quit},
	}
}

var keys = keyMap{
	Up: key.NewBinding(
		key.WithKeys("up", "k"),
		key.WithHelp("↑/k", "move up"),
	),
	Down: key.NewBinding(
		key.WithKeys("down", "j"),
		key.WithHelp("↓/j", "move down"),
	),
	Left: key.NewBinding(
		key.WithKeys("left", "h"),
		key.WithHelp("←/h", "-15 minutes"),
	),
	Right: key.NewBinding(
		key.WithKeys("right", "l"),
		key.WithHelp("→/l", "+15 minutes"),
	),
	Help: key.NewBinding(
		key.WithKeys("?"),
		key.WithHelp("?", "toggle help"),
	),
	Space: key.NewBinding(
		key.WithKeys(" "),
		key.WithHelp("<space>", "(un)select a timezone to compare"),
	),
	Equal: key.NewBinding(
		key.WithKeys("="),
		key.WithHelp("=", "round down to the hour"),
	),
	Quit: key.NewBinding(
		key.WithKeys("q", "esc", "ctrl+c"),
		key.WithHelp("q", "quit"),
	),
}

type option struct {
	Timezone string
	Hour     int
	IsHome   bool
}

func (o option) InHours() bool {
	// 0900-1800 are (standard) working hours
	return o.Hour >= 9 && o.Hour <= 17
}

func (o option) NearOfficeHours() bool {
	// allow ~2 hours of leeway for when people may be around to meet, but not at all expected
	return o.Hour >= 7 && o.Hour <= 19
}

type model struct {
	keys    keyMap
	help    help.Model
	options []option
	hour    int
	minutes int
	cursor  int

	selected map[string]struct{}

	table table.Model
}

func initialModel(t time.Time) model {
	m := model{
		selected: make(map[string]struct{}),
		keys:     keys,
		help:     help.New(),
	}

	m.table = table.New(
		table.WithFocused(true),
		table.WithHeight(9),
		table.WithWidth(70),
	)

	m.hour = t.Hour()
	m.minutes = t.Minute()

	m.updateChoices(m.hour, m.minutes)
	m.updateTable()

	return m
}

func newChoiceForLocation(t time.Time, location string) option {
	loc, err := time.LoadLocation(location)
	if err != nil {
		panic(err)
	}

	return option{
		Timezone: location,
		Hour:     t.In(loc).Hour(),
	}
}

func (m *model) updateChoices(hour, minutes int) {
	t := time.Now().
		// ignore the hours and minutes
		Truncate(24 * time.Hour).
		// and then set them
		Add(time.Duration(hour) * time.Hour).
		Add(time.Duration(minutes) * time.Minute)

	// reset the table rows
	m.options = nil

	locations := []string{
		"US/Pacific",
		"US/Central",
		"US/Eastern",
	}

	for _, l := range locations {
		m.options = append(m.options, newChoiceForLocation(t, l))
	}

	m.options = append(m.options, newChoiceForLocation(t, "UTC"))

	home := newChoiceForLocation(t, "Europe/London")
	home.IsHome = true
	m.options = append(m.options, home)

	locations = []string{
		"Europe/Paris",
		"Europe/Athens",
	}

	for _, l := range locations {
		m.options = append(m.options, newChoiceForLocation(t, l))
	}
}

func (m model) Init() tea.Cmd {
	return nil
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	skipUpdate := false

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch {
		case key.Matches(msg, keys.Quit):
			return m, tea.Quit

			// reset the minutes
		case key.Matches(msg, keys.Equal):
			m.minutes = 0

		case key.Matches(msg, keys.Left):
			m.minutes -= 15
			if m.minutes < 0 {
				m.hour--
				// 60 + -5 = 55
				m.minutes = 60 + m.minutes
			}
			m.hour = m.hour % 24

		case key.Matches(msg, keys.Right):
			m.minutes += 15
			if m.minutes >= 60 {
				m.hour++
			} else if m.minutes > 60 {
				m.minutes = 15
				m.hour++
			}
			m.minutes = m.minutes % 60
			m.hour = m.hour % 24

		case key.Matches(msg, keys.Up):
			if m.cursor > 0 {
				m.cursor--
			}

		case key.Matches(msg, keys.Down):
			if m.cursor < len(m.options)-1 {
				m.cursor++
			}

		case key.Matches(msg, keys.Space):
			sel := m.table.SelectedRow()
			tz := sel[1]

			_, ok := m.selected[tz]
			if ok {
				delete(m.selected, tz)
			} else {
				m.selected[tz] = struct{}{}
			}

			// we want to skip the Table's handling of the space bar, which clashes with what we want to do
			skipUpdate = true

		case key.Matches(msg, keys.Help):
			m.help.ShowAll = !m.help.ShowAll
		}

	}

	m.updateChoices(m.hour, m.minutes)
	m.updateTable()

	var cmd tea.Cmd
	if !skipUpdate {
		m.table, cmd = m.table.Update(msg)
	}

	return m, cmd
}

func (m *model) updateTable() {
	var rows []table.Row

	for _, option := range m.options {
		selected := false
		if _, ok := m.selected[option.Timezone]; ok {
			selected = true
		}

		selectedIndicator := ""
		if selected {
			selectedIndicator = ">"
		}

		suitabilityIndicator := ""
		if selected {
			if option.InHours() {
				suitabilityIndicator = "✅"
			} else if option.NearOfficeHours() {
				suitabilityIndicator = "🥱"
			} else {
				suitabilityIndicator = "🛑"
			}
		}

		rows = append(rows, table.Row{
			selectedIndicator,
			option.Timezone,
			fmt.Sprintf("%02d%02d", option.Hour, m.minutes),
			suitabilityIndicator,
		})
	}

	m.table.SetColumns([]table.Column{
		{
			Title: "",
			Width: 1,
		},
		{
			Title: "Timezone",
			Width: 25,
		},
		{
			Title: "Time",
			Width: 5,
		},
		{
			Title: "🤔",
			Width: 8,
		},
	})

	m.table.SetRows(rows)
}

func (m model) View() string {
	helpView := m.help.View(m.keys)

	return m.table.View() + "\n" + helpView
}

func main() {
	p := tea.NewProgram(initialModel(time.Now().UTC()))
	if _, err := p.Run(); err != nil {
		fmt.Printf("Error: %v\n", err)
		os.Exit(1)
	}
}
